﻿#pragma strict

// 사격제한 시간
private var ftime : float = 0.0f;
private var MAXTime : float = 2;

// 포탄
var bullet : Transform;
// LookAt() 목표
var target : Transform;
var spPoint : Transform;
// 발사 불꽃
var explosion : Transform;
// 발사음
var snd : AudioClip;

// 초당 회전각도
var rotAng = 15;

var power = 2000;

function Start () {

}

function Update ()
 {
    var amtToRot = rotAng * Time.deltaTime;
    transform.RotateAround(Vector3.zero, Vector3.up, amtToRot);

 	// 아군 방향으로 회전 
	transform.LookAt(target);
	// 경과시간 누적.
	ftime += Time.deltaTime;
	
	// 탐색결과 저장 
	var hit : RaycastHit;
	// 포탑의 전방  
	// 로컬좌표.
	//var fwd = Vector3.forward;
	// 글로벌좌표.
	var fwd = transform.TransformDirection(Vector3.forward);
	
	// 탐색실패
	if( false 
	    == Physics.Raycast(spPoint.transform.position, fwd, hit, 20) )
    {
    	return;
    }
    
    // 유저탱크가 아니라면 리턴한다.
    if("Tank" != 
    	hit.collider.gameObject.tag
   		|| MAXTime > ftime )
    { 
       	return;
    }
    
    // 포구 앞의 화염
    var explos =Instantiate(explosion, spPoint.transform.position, Quaternion.identity);
    Destroy(explos, 3);
    
    // 포탄
    var obj = Instantiate(bullet, spPoint.transform.position,Quaternion.identity);
    obj.rigidbody.AddForce(spPoint.transform.forward * power);
    
    if( null != snd )
    {
    	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
    }
    
    //경과시간리셋
    ftime = 0;
}