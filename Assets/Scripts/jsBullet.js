#pragma strict

private var power = 200;

// 사운드 파일
var snd : AudioClip;
var explosion : Transform;

function OnTriggerEnter(coll : Collider)
{
	if( null != snd )
	{
		// 사운드출력
		AudioSource.PlayClipAtPoint(snd, transform.position);
	}
		
	Instantiate(explosion, coll.transform.position, Quaternion.identity);
	// 포탄 제거
	Destroy(gameObject);

	if( coll.name.Contains("Cube") )
	{
        jsScore.hit++; 
    	// 장애물 제거
    	Destroy(coll.gameObject);
	}
    else if("Enemy" == coll.gameObject.tag )
    {
        //점수 증가
        jsScore.hit++; 
        // 5점 이상이면
        if( 5 < jsScore.hit )
        {
             //적군 탱크 파괴
           //  Destroy(coll.transform.root.gameObject);

           //승리 화면으로 분기
           Application.LoadLevel("WinGame");
        }
    }
    else if("Tank" == coll.gameObject.tag )
    {
        //실점 증가
        jsScore.lose++;
        if( 5 < jsScore.lose )
        {
            // 패배화면으로 분기
            Application.LoadLevel("LoseGame");
        }
    }
}

function Start () {

}

function Update () 
{
	Destroy(gameObject,3);
}