﻿#pragma strict

// 이동속도 (m/s)
var speed = 5;
// 회전 속도(m/s)
var rotSpeed = 120;
// 포탑
var turret : GameObject;
var spPoint : Transform;
// 포탄
var bullet : Transform;
// v폭팔
var explosion : Transform;

var power = 2000;

function Update () 
{
	// 프레임에 이동할 거리
	var amtToMove = speed * Time.deltaTime;
	
	//프레임에 회전할 각도
	var amtToRot = rotSpeed * Time.deltaTime;
	
	//전후진
	var front = Input.GetAxis("Vertical");
	//좌우 회전 방향
	var ang = Input.GetAxis("Horizontal");
	//포탑 회전방향
	var ang2 = Input.GetAxis("MyTank");
	
	transform.Translate(Vector3.forward * front* amtToMove);
	transform.Rotate(Vector3(0, ang * amtToRot, 0));
	turret.transform.Rotate(Vector3.up * ang2 * amtToRot);
	
	if( Input.GetButtonDown("Fire1") )
	{
		var myBullet = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
        myBullet.rigidbody.AddForce(spPoint.transform.forward * power);

        myBullet = Instantiate(explosion, spPoint.transform.position, Quaternion.identity);
        Destroy(myBullet, 3);
	}
}